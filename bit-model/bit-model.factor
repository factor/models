! Lets make a bit model 
! make a variable that has a model for every bit
! Copyright (C) 2022 forthnutter.

USING: accessors arrays assocs bit-arrays bit-arrays.private
    byte-arrays 
    hashtables kernel literals
    math math.bitwise models namespaces parser
    sequences sequences.private tools.continuations
    vectors ;

IN: models.bit-model

TUPLE: model-bit < model bitnumber ;

: <model-bit> ( n -- model-bit )
    f model-bit new-model swap
    >>bitnumber ;


TUPLE: bit-model < model models ;


: bit-model-init ( bit-model -- bit-model' )
    [
        dup
        models>>
        [
            swap drop
            <model-bit>
        ] map-index
        swap models<<
    ] keep ;

: bit-add-connection ( obsever bit-model -- )
    models>>
    [
        [ dup ] dip
        add-connection
    ] each drop ;

: bit-add-dependency ( models model -- )
    swap
    [
        [ dup ] dip
        swap
        add-dependency
    ] each drop ;
    

M: bit-model update-model
    break
    [ value>> integers>bits ] keep
    swap
    [ ] each ;


: <bit-model> ( n -- bit-model )
    dup 0 < [ bad-array-length ] when
    f bit-model new-model swap
    f <array> >>models 
    bit-model-init ;