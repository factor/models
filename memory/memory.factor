! Copyright (C) 2016 Joseph Moschini.
! See http://factorcode.org/license.txt for BSD license.
!


USING: models kernel accessors sequences math ;

IN: models.memory


CONSTANT: CPU-ADDRESS-ERROR 12

! model data is the address
! data  is the array of data from or to memory
! n     number of bytes to read
! err   memory error
TUPLE: memory < model data n err ;


GENERIC: read ( n address model -- array )
GENERIC: write ( array address model -- )

M: memory read
    swap [ swap >>n ] dip swap
    [ set-model ] keep data>> ;

: memory-read ( n address model -- array )
    f >>data ! make sure data is false for reading
    t >>err   ! error is false until we complete read
    swap [ swap >>n ] dip swap
    [ set-model ] keep data>> ;


M: memory write
[ swap drop data<< ] 2keep set-model ;

: memory-error ( model -- )
  [ err>> ] keep swap
  [ CPU-ADDRESS-ERROR >>state drop ]
  [ drop ] if ;



: memory-write ( array address model -- ? )
    [ [ drop ] dip data<< ] 2keep
    t >>err   ! error is false until we complete write
    [ set-model ] keep [ memory-error ] keep data>> ;

: memory-data ( model -- model data )
    [ data>> ] keep swap ;

! if we have data then we are in write mode
: memory-write? ( model -- model ? )
    memory-data f = not ;

: memory-address ( model -- address )
    value>> ;

: memory-nbytes ( model -- nbytes )
    n>> ;

: memory-nbytes-address ( model -- nbytes address )
  [ memory-nbytes ] [ memory-address ] bi ;

! means memory read or write was ok
: memory-ok ( model -- )
  f >>err drop ;

! add memory obsever to model
: memory-add ( memory obsever -- memory )
    swap [ add-connection ] keep f >>data 0 >>n f >>err ;
