
USING: arrays generic kernel math models namespaces sequences assocs
      tools.test tools.continuations models models.memory accessors prettyprint ;

IN: models.mtest

TUPLE: tmem array start ;

M: tmem model-changed
  break
  [ dup data>> ] dip swap
  [
    ! write to memory

  ]
  [
    ! read from memory
    [ dup [ n>> ] [ value>> ] bi ] dip  . . . .
  ] if



: <tmem> ( array start -- tmem )
  tmem boa ;


: tmem-start ( -- )
  16 f <array> 1 <tmem> <memory> [ add-connection ] keep "mem" set ;
