! Copyright (C) 2016 Joseph Moschini.  a.k.a. forthnutter
! See http://factorcode.org/license.txt for BSD license.
!

USING: accessors kernel math math.bitwise math.order math.parser
      tools.continuations models
      prettyprint sequences byte-arrays
      namespaces ;

IN: models.clock



TUPLE: clock < model count ;

: <clock> ( -- clock )
    0 clock new-model 0 >>count ;

: new-clock ( model -- clock )
    0 swap new-model 0 >>count ;
    
! set clock to 1 does not effect count
: clock-high ( clock -- )
    1 swap set-model ;


: clock-low ( clock -- )
    0 swap set-model ;

: clock-status ( clock -- ? )
    value>> 1 = ;

: clock-inc ( clock -- )
    [ count>> 1 + ] keep count<< ;

    
: clock-toggle ( clock -- )
    [ [ clock-status ] keep swap [ clock-low ] [ clock-high ] if ] keep clock-inc ;

: clock-add ( object clock -- )
    add-connection ;

